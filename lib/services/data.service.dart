import 'dart:convert';

import 'package:exam_patra/models/chart.model.dart';
import 'package:exam_patra/models/home.model.dart';
import 'package:exam_patra/models/user.model.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';

class DataService {
  DataService();

  // Fetch content from the json file
  Future<HomeModel> readJsonforHome() async {
    final String response =
        await rootBundle.loadString('assets/home_page.json');
    dynamic objJson = jsonDecode(response);
    return HomeModel.fromJson(objJson);
  }

  Future<List<ChartModel>> readJsonforGold() async {
    final String response =
        await rootBundle.loadString('assets/Gold_Yearly.json');
    dynamic objJson = jsonDecode(response);
    var arrayJson = objJson as List;
    return arrayJson.map((objJson) => ChartModel.fromJson(objJson)).toList();
  }

  // Fetch content from the internet
  Future<UserModel?> getUser() async {
    try {
      // final Map<String, dynamic> queryParams = {  };
      // String queryString = Uri(queryParameters: queryParams).query;

      final response = await http.get(
          Uri.parse('https://reqres.in/api/users/2'),
          headers: {'Content-Type': 'application/json; charset=UTF-8'});

      if (response.statusCode == 200) {
        dynamic jsonDec = jsonDecode(utf8.decode(response.bodyBytes));
        return UserModel.fromJson(jsonDec);
      } else {
        return null;
      }
    } on Exception catch (_) {
      return null;
    }
  }

  // Save and Fetch content from local storage
  String storageKey = 'exam-patra';
  String dataKey = 'user-info';

  Future<UserInfoModel?> getUserInfo() async {
    try {
      final LocalStorage storage = LocalStorage(storageKey);
      final isReady = await storage.ready;
      if (isReady) {
        Map<String, dynamic> data = jsonDecode(storage.getItem(dataKey));
        return UserInfoModel.fromJson(data);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<bool> setUserInfo(UserInfoModel userData) async {
    try {
      final LocalStorage storage = LocalStorage(storageKey);
      final isReady = await storage.ready;
      if (isReady) {
        await storage.setItem(dataKey, jsonEncode(userData.toJSONEncodable()));
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }
}
