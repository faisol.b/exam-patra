import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF6A63E2);
const kPrimaryLightColor = Color(0xFFE3F2FD);

final ThemeData appTheme = ThemeData(
  fontFamily: 'DBHeavent',
  primaryColor: kPrimaryColor,
  scaffoldBackgroundColor: Colors.white,
);
