import 'package:exam_patra/models/chart.model.dart';
import 'package:exam_patra/services/data.service.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class ChartScreen extends StatefulWidget {
  const ChartScreen({Key? key}) : super(key: key);

  @override
  _ChartScreenState createState() => _ChartScreenState();
}

class _ChartScreenState extends State<ChartScreen> {
  DataService service = DataService();
  late List<ChartModel> data;
  bool isInit = true;

  loadData() async {
    var response = await service.readJsonforGold();
    setState(() {
      data = response; //.where((e) => e.year >= 1994).toList();
      isInit = false;
    });
  }

  @override
  void initState() {
    loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return isInit
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.only(left: 15, right: 15, top: 50),
              alignment: Alignment.topLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Chart Display',
                    style: TextStyle(
                      fontFamily: 'DBHeavent',
                      fontSize: 36,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.42,
                    ),
                  ),
                  const Text(
                    'Glod_Yearly.json',
                    style: TextStyle(
                      fontFamily: 'DBHeavent',
                      fontSize: 28,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.42,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  SfCartesianChart(
                    plotAreaBorderWidth: 0,
                    primaryXAxis: NumericAxis(
                      majorGridLines: const MajorGridLines(width: 0),
                      //interval: 4,
                    ),
                    primaryYAxis: NumericAxis(isVisible: false),
                    series: <ChartSeries>[
                      CandleSeries<ChartModel, double>(
                          showIndicationForSameValues: false,
                          enableSolidCandles: false,
                          bearColor: const Color(0xFFFF0000),
                          bullColor: const Color(0xFFB5F79F),
                          dataSource: data,
                          xValueMapper: (ChartModel data, _) => data.year,
                          highValueMapper: (ChartModel data, _) => data.high,
                          lowValueMapper: (ChartModel data, _) => data.low,
                          openValueMapper: (ChartModel data, _) => data.open,
                          closeValueMapper: (ChartModel data, _) => data.close),
                    ],
                  ),
                ],
              ),
            ),
          );
  }
}
