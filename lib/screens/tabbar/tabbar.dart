import 'package:exam_patra/screens/chart/chart.dart';
import 'package:exam_patra/screens/home/home.dart';
import 'package:exam_patra/screens/profile/profile.dart';
import 'package:exam_patra/theme.dart';
import 'package:flutter/material.dart';

class TabbarScreen extends StatefulWidget {
  const TabbarScreen({Key? key, this.pageIndex = 0}) : super(key: key);

  final int pageIndex;

  @override
  _TabbarScreenState createState() => _TabbarScreenState();
}

class _TabbarScreenState extends State<TabbarScreen> {
  int _selectedIndex = 0;
  Future<void> _onItemTapped(int index) async {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    _onItemTapped(widget.pageIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: <Widget>[
        const HomeScreen(),
        const ChartScreen(),
        const ProfileScreen(),
      ][_selectedIndex],
      bottomNavigationBar: buildBottomNavigationBar(),
    );
  }

  BottomNavigationBar buildBottomNavigationBar() {
    const double iconSize = 30;

    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined, size: iconSize),
            activeIcon: Icon(Icons.home_outlined, size: iconSize),
            label: ''),
        BottomNavigationBarItem(
            icon: Icon(Icons.widgets_outlined, size: iconSize),
            activeIcon: Icon(Icons.widgets_outlined, size: iconSize),
            label: ''),
        BottomNavigationBarItem(
            icon: Icon(Icons.person_outline, size: iconSize),
            activeIcon: Icon(Icons.person_outline, size: iconSize),
            label: ''),
      ],
      type: BottomNavigationBarType.fixed,
      currentIndex: _selectedIndex,
      elevation: 4,
      selectedItemColor: kPrimaryColor,
      unselectedItemColor: const Color(0xffbab9d0),
      showSelectedLabels: false,
      showUnselectedLabels: false,
      backgroundColor: const Color(0xFFF4F4F4),
      onTap: _onItemTapped,
    );
  }
}
