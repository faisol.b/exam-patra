import 'package:exam_patra/models/user.model.dart';
import 'package:exam_patra/services/data.service.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  DataService service = DataService();
  late UserInfoModel data;
  bool isInit = true;

  loadData() async {
    var response = await service.getUser();

// save to local storage
    bool isSaveSuccess = await service.setUserInfo(response!.data);
    if (isSaveSuccess) {
      // get from local storage
      var _data = await service.getUserInfo();
      if (_data != null) {
        setState(() {
          data = _data;
          isInit = false;
        });
      }
    }
  }

  @override
  void initState() {
    loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return isInit
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.only(left: 15, right: 15, top: 50),
              alignment: Alignment.topLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'User information',
                    style: TextStyle(
                      fontFamily: 'DBHeavent',
                      fontSize: 36,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.42,
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(vertical: 25),
                    alignment: Alignment.center,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.network(
                        data.avatar,
                        width: 180,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Text(
                    'Name: ${data.firstName} ${data.lastName}',
                    style: const TextStyle(
                      fontFamily: 'DBHeavent',
                      fontSize: 28,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.42,
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Email: ${data.email}',
                    style: const TextStyle(
                      fontFamily: 'DBHeavent',
                      fontSize: 28,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.42,
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}
