import 'package:exam_patra/models/home.model.dart';
import 'package:exam_patra/services/data.service.dart';
import 'package:exam_patra/theme.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int typeSelected = 0;

  TextEditingController formController = TextEditingController();
  String keyword = '';

  DataService service = DataService();
  late HomeModel data;
  bool isInit = true;

  loadData() async {
    var response = await service.readJsonforHome();
    setState(() {
      data = response;
      isInit = false;
    });
  }

  @override
  void initState() {
    loadData();
    super.initState();
  }

  findout() {
    setState(() {
      keyword = formController.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return isInit
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.only(left: 15, right: 15, top: 50),
              alignment: Alignment.topLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${data.title},\n${data.subTitle}',
                    style: const TextStyle(
                      fontFamily: 'DBHeavent',
                      fontSize: 36,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.42,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  searchCard(),
                  const SizedBox(
                    height: 15,
                  ),
                  Text(
                    data.quickAccessTitle,
                    style: const TextStyle(
                      fontFamily: 'DBHeavent',
                      fontSize: 30,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.42,
                    ),
                  ),
                  Wrap(
                    children: List.generate(data.quickAccess.length, (index) {
                      return choiceChip(
                          data.quickAccess[index].menuTitle, index);
                    }),
                  ),
                  itemListTile(),
                ],
              ),
            ),
          );
  }

  Card searchCard() {
    return Card(
      color: const Color(0xFFDAEEFF),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(25),
              ),
              child: TextFormField(
                controller: formController,
                keyboardType: TextInputType.text,
                decoration: const InputDecoration(
                  icon: Icon(
                    Icons.search,
                    size: 30,
                  ),
                  hintText: 'Search your file',
                  isDense: true,
                  contentPadding: EdgeInsets.fromLTRB(0, 4, 4, 4),
                  filled: true,
                  fillColor: Colors.white,
                  border: InputBorder.none,
                ),
                style: const TextStyle(
                  fontFamily: 'DBHeavent',
                  color: Color(0xff29303e),
                  fontSize: 24,
                  fontWeight: FontWeight.w300,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0.54,
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              'Lorem ipsum dolor sit amet, consectetur adipiscingelit, sed do eiusmod tempor incididunt ut labore etdolore magna aliqua.',
              style: TextStyle(
                  fontFamily: 'DBHeavent',
                  color: Colors.grey,
                  fontSize: 20,
                  fontWeight: FontWeight.w300,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0.54,
                  height: 1),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: findout,
              style: ElevatedButton.styleFrom(
                primary: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              child: const Text(
                'Find out more',
                style: TextStyle(
                    fontFamily: 'DBHeavent',
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w300,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0.54,
                    height: 1),
              ),
            )
          ],
        ),
      ),
    );
  }

  ChoiceChip choiceChip(String label, int number) {
    bool isSelected = typeSelected == number;

    return ChoiceChip(
      label: Text(
        label,
        style: TextStyle(
          fontFamily: 'DBHeavent',
          color: isSelected ? Colors.white : Colors.black,
          fontSize: 20,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
          letterSpacing: 0.42,
        ),
      ),
      backgroundColor: Colors.white,
      selectedColor: kPrimaryColor,
      selected: isSelected,
      onSelected: (bool selected) {
        setState(() {
          typeSelected = selected ? number : 0;
        });
      },
    );
  }

  Widget itemListTile() {
    List<QuickAccessItemModel> items;

    if (keyword.isNotEmpty) {
      items = data.quickAccess[typeSelected].item
          .where((e) => e.itemName.contains(keyword))
          .toList();
    } else {
      items = data.quickAccess[typeSelected].item;
    }

    return Column(
      children: List.generate(items.length, (index) {
        QuickAccessItemModel item = items[index];

        return Card(
          color: hexColor(item.itemBgColor),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: ListTile(
            leading: Card(
              child: Container(
                padding: const EdgeInsets.all(8),
                child: Image(image: NetworkImage(item.itemImg)),
              ),
            ),
            title: Text(
              item.itemName,
              style: const TextStyle(
                fontFamily: 'DBHeavent',
                fontSize: 20,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0.42,
              ),
            ),
            isThreeLine: true,
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  item.itemDecription,
                  style: const TextStyle(
                    fontFamily: 'DBHeavent',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0.42,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  width: 40,
                  height: 5,
                  decoration: BoxDecoration(
                    color: hexColor(item.itemColor),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  Color hexColor(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }
}
