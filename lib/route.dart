import 'package:flutter/material.dart';

import 'screens/tabbar/tabbar.dart';

Map<String, Widget Function(dynamic)> appRoute = {
  '/home': (context) => const TabbarScreen(),
  '/chart': (context) => const TabbarScreen(pageIndex: 1),
  '/profile': (context) => const TabbarScreen(pageIndex: 2),
};
