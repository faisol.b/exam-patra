class UserModel {
  UserModel({required this.data, required this.support});

  final UserInfoModel data;
  final UserSupportModel support;

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      data: UserInfoModel.fromJson(json['data']),
      support: UserSupportModel.fromJson(json['support']),
    );
  }
}

class UserInfoModel {
  UserInfoModel(
      {required this.id,
      required this.email,
      required this.firstName,
      required this.lastName,
      required this.avatar});

  final int id;
  final String email;
  final String firstName;
  final String lastName;
  final String avatar;

  factory UserInfoModel.fromJson(Map<String, dynamic> json) {
    return UserInfoModel(
      id: json['id'],
      email: json['email'],
      firstName: json['first_name'],
      lastName: json['last_name'],
      avatar: json['avatar'],
    );
  }

  toJSONEncodable() {
    Map<String, dynamic> m = {};
    m['id'] = id;
    m['email'] = email;
    m['first_name'] = firstName;
    m['last_name'] = lastName;
    m['avatar'] = avatar;

    return m;
  }
}

class UserSupportModel {
  UserSupportModel({required this.url, required this.text});

  final String url;
  final String text;

  factory UserSupportModel.fromJson(Map<String, dynamic> json) {
    return UserSupportModel(
      url: json['url'],
      text: json['text'],
    );
  }
}
