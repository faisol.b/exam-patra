class HomeModel {
  HomeModel(
      {required this.title,
      required this.subTitle,
      required this.quickAccessTitle,
      required this.quickAccess});

  final String title;
  final String subTitle;
  final String quickAccessTitle;
  final List<QuickAccessModel> quickAccess;

  factory HomeModel.fromJson(Map<String, dynamic> json) {
    return HomeModel(
      title: json['title'],
      subTitle: json['sub_title'],
      quickAccessTitle: json['quick_access_title'],
      quickAccess: (json['quick_access'] as List)
          .map((e) => QuickAccessModel.fromJson(e))
          .toList(),
    );
  }
}

class QuickAccessModel {
  QuickAccessModel({required this.menuTitle, required this.item});

  final String menuTitle;
  final List<QuickAccessItemModel> item;

  factory QuickAccessModel.fromJson(Map<String, dynamic> json) {
    return QuickAccessModel(
      menuTitle: json['menu_title'],
      item: (json['item'] as List)
          .map((e) => QuickAccessItemModel.fromJson(e))
          .toList(),
    );
  }
}

class QuickAccessItemModel {
  QuickAccessItemModel(
      {required this.itemName,
      required this.itemDecription,
      required this.itemImg,
      required this.itemColor,
      required this.itemBgColor});

  final String itemName;
  final String itemDecription;
  final String itemImg;
  final String itemColor;
  final String itemBgColor;

  factory QuickAccessItemModel.fromJson(Map<String, dynamic> json) {
    return QuickAccessItemModel(
      itemName: json['item_name'],
      itemDecription: json['item_decription'],
      itemImg: json['item_img'],
      itemColor: json['item_color'],
      itemBgColor: json['item_bg_color'],
    );
  }
}
