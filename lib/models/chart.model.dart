class ChartModel {
  ChartModel(
      {required this.year,
      required this.avgClosingPrice,
      required this.open,
      required this.high,
      required this.low,
      required this.close,
      required this.percentChange});

  final double year;
  final double avgClosingPrice;
  final double open;
  final double high;
  final double low;
  final double close;
  final double percentChange;

  factory ChartModel.fromJson(Map<String, dynamic> json) {
    return ChartModel(
      year: toDouble(json['year'].toString()),
      avgClosingPrice: toDouble(json['avg_closing_price'].toString()),
      open: toDouble(json['open'].toString()),
      high: toDouble(json['high'].toString()),
      low: toDouble(json['low'].toString()),
      close: toDouble(json['close'].toString()),
      percentChange: toDouble(json['percent_change'].toString()),
    );
  }

  static double toDouble(String x) {
    if (x.contains('.')) {
      return double.parse(x);
    } else {
      return double.parse(x + '.0');
    }
  }
}
